<?php
return [

    /*
    * Default email theme
    */
    'theme' => 'cognito',

    /*
      * Email Optimization - will strip and minify html of email before rendering/sending
      */
    'optimization' => true,

    'themes' => [
        'cognito' => [
            'button' => [
                'color' => '#3273dc',
                'hoverColor' => '#276cda',
            ],
            'hr' => [
                'color' => '#555555'
            ],
            'email' => [
                'backgroundColor' => '#f6f6f6',
                'bodyBackgroundColor' => '#f6f6f6',
            ],
            'webfont' => [
                'url' => 'https://fonts.googleapis.com/css?family=Roboto',
                'name' => 'Roboto'
            ],
            'fullBleedTop' => [
                'show' => false,
            ],
            'fullBleedBottom' => [
                'show' => false,
            ],
            'header' => [
                'show' => false,
            ],
            'footer' => [
                'show' => false,
            ]
        ]
    ],

    /*
     * Branding Details for footer
     */
    'company' => [
        'name' => 'Cognito',
        'address' => '',
        'telephone' => '',
        'website' => 'http://cognito.hireme.co.za',
    ],

    /*
    * Always try to embed images . Default true
    */
    'embed_images' => true,

    /*
      * Strict - will throw error if setting is not found
      */
    'strict' => true,

];