<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (\App::environment('production')) {
    $middleware = [\HTMLMin\HTMLMin\Http\Middleware\MinifyMiddleware::class];
} else {
    $middleware = [];
}

Route::group(['middleware' => $middleware], function() {

    Route::get('/', ['uses' => 'HomeController@index', 'as' => 'index']);

    Route::group(['middleware' => 'auth'], function () {

        Route::post('/token', ['uses' => 'HomeController@token', 'as' => 'token']);

        Route::post('/users/{any}', ['uses' => 'UsersController@vueHelper', 'as' => 'users']);

    });

});

// Catching all undeclared routes and redirecting to vue routes
Route::get('/{any}', ['uses'=>'HomeController@any'])->where('any', '(.*)');