<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendUserPasswordResetEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $token;
    private $email;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $token)
    {
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->email;
        $token = $this->token;

        $user = \App\User::findByEmail($email);

        $mail = new \Wai\Emails\Email('emails.auth.reset-password');

        $from['name'] = config('mail.from.name');
        $from['email'] = config('mail.from.address');

        $mail->send(['token' => $token, 'email' => $user->email], function ($m) use ($user, $from) {
            $m->to($user->email, $user->name);
            $m->replyTo($from['email'], $from['name']);
            $m->subject('Reset your password');
        });
    }
}
