<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendUserVerificationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $email;
    private $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $id)
    {
        $this->email = $email;
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = $this->email;

        $url = \URL::temporarySignedRoute(
            'verification.verify', \Carbon\Carbon::now()->addMinutes(60), ['id' => $this->id]
        );

        $user = \App\User::findByEmail($email);

        $mail = new \Wai\Emails\Email('emails.auth.verification');

        $from['name'] = config('mail.from.name');
        $from['email'] = config('mail.from.address');

        $mail->send(['url' => $url, 'email' => $user->email], function ($m) use ($user, $from) {
            $m->to($user->email, $user->name);
            $m->replyTo($from['email'], $from['name']);
            $m->subject('Verify email address');
        });
    }
}
