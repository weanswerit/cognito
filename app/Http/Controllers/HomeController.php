<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    public function index()
    {
        if(\Auth::user()) {
            return view('index');
        } else {
            return redirect()->route('login');
        }
    }

    public function any()
    {
        if(\Auth::user()) {
            return view('index');
        } else {
            return redirect()->route('login')->with('redirect', request()->getRequestUri());
        }
    }

    public function token()
    {
        return response()->json(['token'=>csrf_token()]);
    }
}
