<?php

namespace App\Http\Controllers;

class VueController extends Controller
{
    protected $class;
    protected $keyName='id';
    protected $itemName='item';
    protected $orderAsc=true;
    protected $orderBy=null;
    protected $itemsPerPage=10;
    protected $logItemsPerPage=10;

    protected $singleRelationships = [];
    protected $multipleRelationships = [];

    protected $singleAppends = [];
    protected $multipleAppends = [];

    public function vueHelper($action=null)
    {
        if(is_null($action) and request()->has('action')) {
            $action = request()->get('action');
        }

        if($action) {
            if(in_array($action, get_class_methods($this))) {
                return $this->$action();
            } else {
                throw new \Exception('Method not found');
            }
        } else {
            return response('Not acceptable', 406);
        }
    }

    public function create()
    {
        $item = request($this->itemName);
        $i = new $this->class;
        foreach($item as $param => $value) {
            if(!in_array($param, $this->singleAppends)) {
                $i->$param = $value;
            }
        }
        $i->save();

        foreach($item as $param => $value) {
            if(in_array($param, $this->singleAppends)) {
                $i->$param = $value;
            }
        }

        if(method_exists($this, 'afterCreate')) {
            $this->afterCreate($i);
        }

        return response()->json([
            $this->keyName=>$i->id
        ]);
    }

    public function get()
    {
        return response()->json([
            $this->itemName => $this->class::with($this->singleRelationships)
                ->find(request($this->keyName))
                ->append($this->singleAppends)
        ]);
    }

    public function update()
    {
        $item = request($this->itemName);
        $i = $this->class::with($this->singleRelationships)
            ->find($item[$this->keyName]);

        $allowedParams = array_keys($i->getAttributes());

        foreach($item as $param=>$value) {
            if(in_array($param, $allowedParams) or in_array($param, $this->singleAppends)) {
                $i->$param = $value;
            }
        }

        $i->update();

        $changes = $i->getChanges();
        if($i->timestamps and isset($changes['updated_at'])) {
            unset($changes['updated_at']);
        }

        $i->append($this->singleAppends);

        if(method_exists($this, 'afterUpdate')) {
            $this->afterUpdate($i, $changes);
        }

        return response()->json([
            $this->itemName=>$i,
            'changes'=>$changes
        ]);
    }

    public function delete()
    {
        $this->class::find(request($this->keyName))
            ->delete();

        if(method_exists($this, 'afterDelete')) {
            $this->afterDelete($i);
        }

        return response()->json([
            'message'=>ucfirst($this->itemName).' deleted'
        ]);
    }

    public function getAll()
    {
        if(!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        return response()->json([
            str_plural($this->itemName) => $this->class::with($this->multipleRelationships)
                ->get()
        ]);
    }

    public function getPaginated()
    {
        if(!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        $query = $this->class::query();
        $query->with($this->multipleRelationships);
        $orderBy = $this->orderBy ? $this->orderBy : $this->keyName ;
        if($this->orderAsc) {
            $query->orderBy($orderBy, 'ASC');
        } else {
            $query->orderBy($orderBy, 'DESC');
        }

        return response()->json($query->paginate($this->itemsPerPage));
    }

    public function getSearchQuery()
    {
        if(method_exists($this, 'searchQuery')) {
            $results = $this->searchQuery();
        } else {
            throw new \Exception('No searchQuery method defined');
        }

        $orderBy = $this->orderBy ? $this->orderBy : $this->keyName;
        if($this->orderAsc) {
            $results->orderBy($orderBy, 'ASC');
        } else {
            $results->orderBy($orderBy, 'DESC');
        }

        return $results;
    }

    public function search()
    {
        return response()->json([
            str_plural($this->itemName) => $this->getSearchQuery()->get()
        ]);
    }

    public function searchPaginated()
    {
        if(!empty($this->multipleAppends)) {
            $this->class::setStaticAppends($this->multipleAppends);
        }

        return response()->json([
            str_plural($this->itemName) => $this->getSearchQuery()->paginate($this->itemsPerPage)
        ]);
    }

    public function logs()
    {
        return response()->json([
            'logs' => $this->class::find(request($this->keyName))
                ->logs()
                ->orderBy('id', 'DESC')
                ->get()
        ]);
    }

    public function logsPaginated()
    {
        return response()->json($this->class::find(request($this->keyName))
            ->logs()
            ->orderBy('id', 'DESC')
            ->paginate($this->logItemsPerPage));
    }

}
