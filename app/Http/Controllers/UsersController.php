<?php

namespace App\Http\Controllers;

class UsersController extends VueController
{
    protected $class = \App\User::class;
    protected $itemName = 'user';

    protected $orderAsc = false;
    protected $orderBy = 'created_at';
    protected $itemsPerPage = 15;

    protected $singleAppends = [];
    protected $multipleAppends = [];

    protected $singleRelationships = [];
    protected $multipleRelationships = [];

    public function getUser()
    {
        return response()->json([$this->itemName => $this->class::with($this->singleRelationships)
            ->find(\Auth::id())
            ->append($this->singleAppends)]);
    }
}
