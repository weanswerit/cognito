<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const VIEW          = 1;
    const SALES         = 2;
    const MANAGER       = 4;
    const ADMIN         = 8;
    const SUPER_ADMIN   = 9;

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_role');
    }
}
