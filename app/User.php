<?php

namespace App;

use Laravolt\Avatar\Avatar;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Traits\DynamicChangeAppends;
    use Notifiable;
    use SoftDeletes;

    protected $fillable = [
        'name', 'email', 'password', 'details'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'avatar'
    ];

    public function sendPasswordResetNotification($token)
    {
        dispatch(new \App\Jobs\SendUserPasswordResetEmail($this->email, $token));
    }

    public function sendEmailVerificationNotification()
    {
        dispatch(new \App\Jobs\SendUserVerificationEmail($this->email, $this->id));
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function getDetailsAttribute()
    {
        $details = json_decode($this->attributes['details'], true);

        if (!isset($details['avatar'])) $details['avatar'] = '';
        if (!isset($details['surname'])) $details['surname'] = '';

        return $details;
    }

    public function setDetailsAttribute($value)
    {
        if(!isset($this->attributes['details'])) {
            $details=[];
        } else {
            $details = json_decode($this->attributes['details'], true);
        }

        foreach($value as $p=>$v) {
            $details[$p] = $v;
        }
        $this->attributes['details']=json_encode($details);
    }

    public function getIdentifierAttribute()
    {
        return $this->getIdentifier();
    }

    public function getIdentifier()
    {
        return $this->id.md5($this->id.'-'.$this->last_login);
    }

    public function getAvatarAttribute()
    {

        $details = $this->getDetailsAttribute();
        if(isset($details['avatar']) and !empty($details['avatar'])) {
            $avatarPath = \ColorTools\Store::getUrl($details['avatar'], function(\ColorTools\Image $image) {
                $image->fit(240, 240);
            });
            return url($avatarPath);
        } else {
            $avatar = new Avatar(config('avatar'));
            return $avatar->create($this->getFullNames())->toBase64()->encoded;
        }
    }

    public function getFullNamesAttribute()
    {
        return $this->getFullNames();
    }

    public function getFullNames()
    {
        return $this->name . ' ' . $this->getDetailsAttribute()['surname'];
    }

    public static function findByEmail($email)
    {
        return self::query()->where('email', $email)->first();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role');
    }

    public function getRolesIdsAttribute()
    {
        return $this->roles()->pluck('id');
    }

    public function setRolesIdsAttribute($rolesIds)
    {
        return $this->roles()->sync($rolesIds);
    }
}
