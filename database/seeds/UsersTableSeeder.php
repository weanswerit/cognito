<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'email'=>'admin@admin.com',
            'password'=>'admins',
            'details'=>[],
            'name' => 'admin',
        ])->roles()->attach([\App\Role::VIEW, \App\Role::MANAGER, \App\Role::SUPER_ADMIN]);
    }
}
