<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles[] = ['id'=>1, 'name'=>'View', 'description'=>'View only'];
        $roles[] = ['id'=>2, 'name'=>'Sales', 'description'=>'Sales executive'];
        $roles[] = ['id'=>4, 'name'=>'Manager', 'description'=>'Manager rights'];
        $roles[] = ['id'=>8, 'name'=>'Admin', 'description'=>'Admin rights'];
        $roles[] = ['id'=>9, 'name'=>'Super admin', 'description'=>'Super dooper'];

        \App\Role::unguarded(function() use ($roles) {
            foreach($roles as $role) {
                \App\Role::create($role);
            }
        });

    }
}
