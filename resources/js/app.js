require('./helpers/pre-loader');
window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': document.querySelector("meta[name='csrf-token']").getAttribute("content"),
    'X-Requested-With': 'XMLHttpRequest'
};
window.moment = require('moment');
window.Pusher = require('pusher-js');
import Echo from "laravel-echo"
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    wsHost: window.location.hostname,
    wsPort: 6001,
    disableStats: true,
});

import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store';

import Vuetify from 'vuetify'
Vue.use(Vuetify, {
    iconfont: 'mdi'
});

import Toasted from 'vue-toasted';
Vue.use(Toasted);

Vue.mixin({
    methods: {
        slugify(string) {
            const a = 'àáäâãåèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;';
            const b = 'aaaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------';
            const p = new RegExp(a.split('').join('|'), 'g');
            return string.toString().toLowerCase().trim()
                .replace(/\s+/g, '-')
                .replace(p, c => b.charAt(a.indexOf(c)))
                .replace(/&/g, '-and-')
                .replace(/[^\w\-]+/g, '')
                .replace(/\-\-+/g, '-')
                .replace(/^-+/, '');
        },
        ucfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        decodeBase64(string) {
            var e={},i,b=0,c,x,l=0,a,r='',w=String.fromCharCode,L=string.length;
            var A="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
            for(i=0;i<64;i++){e[A.charAt(i)]=i;}
            for(x=0;x<L;x++){
                c=e[string.charAt(x)];b=(b<<6)+c;l+=6;
                while(l>=8){((a=(b>>>(l-=8))&0xff)||(x<(L-2)))&&(r+=w(a));}
            }
            return r;
        },
        isEmpty(obj) {
            for(var key in obj) {
                if(obj.hasOwnProperty(key)) {
                    return false;
                }
            }
            return true;
        }
    }
});

Vue.filter('fromNow', function(date) {
    return new moment(date).fromNow();
});

Vue.filter('date', function(date) {
    return new moment(date).format('YYYY-MM-DD');
});

Vue.filter('datetime', function(date) {
    return new moment(date).format('YYYY-MM-DD HH:mm');
});

Vue.filter('time', function(date) {
    return new moment(date).format('HH:mm');
});

if (process.env.NODE_ENV == 'production') {
    Vue.config.devtools = false;
    Vue.config.debug = false;
    Vue.config.silent = true;
}

new Vue({
    data() {
        return {
        }
    },
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App },
    mounted() {
    },
    created() {
    },
});