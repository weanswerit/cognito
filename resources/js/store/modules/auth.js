export default {
    namespaced: true,

    state: {
        token: '',
        user: {},
    },

    getters: {
        isAuthenticated(state) {
            for(var key in state.user) {
                if(state.user.hasOwnProperty(key)) {
                    return true;
                }
            }
            return false;
        },
        getUser(state) {
            return state.user;
        },
        getToken(state) {
            return state.token;
        }
    },

    actions: {
        getUser(context) {
            return new Promise((resolve, reject) => {
                window.axios.post('/users/getUser').then(response => {
                    context.commit('setUser', response.data.user);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                })
            });
        },
        setToken(context, token) {
            context.commit('setLaravelToken', token);
        },
        logout(context) {
            return new Promise((resolve, reject) => {
                window.axios.post('/logout').then(response => {
                    context.commit('unsetUser');
                    context.dispatch('setToken', response.data.token);
                    window.axios.defaults.headers.common = {
                        'X-CSRF-TOKEN': response.data.token,
                        'X-Requested-With': 'XMLHttpRequest'
                    };
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            })
        }
    },

    mutations: {
        setLaravelToken(state, token) {
            state.token = token;
        },
        setUser(state, user) {
            state.user = user;
        },
        unsetUser(state) {
            state.user = {}
        }
    }
}