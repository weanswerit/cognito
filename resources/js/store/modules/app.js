export default {
    namespaced: true,

    state: {
        drawer: true,
        miniVariant: false,
        rightDrawer: false,
    },

    getters: {
        getDrawerStatus(state) {
            return state.drawer;
        },
        getMiniVariantStatus(state) {
            return state.miniVariant;
        },
        getRightDrawerStatus(state) {
            return state.rightDrawer;
        }
    },

    actions: {
        changeDrawerStatus(context, status) {
            context.commit('setDrawerStatus', status);
        },
        changeMiniVariantStatus(context, status) {
            context.commit('setMiniVariantStatus', status);
        },
        changeRightDrawerStatus(context, status) {
            context.commit('setRightDrawerStatus', status);
        },
    },

    mutations: {
        setDrawerStatus(state, status) {
            state.drawer = status;
        },
        setMiniVariantStatus(state, status) {
            state.miniVariant = status;
        },
        setRightDrawerStatus(state, status) {
            state.rightDrawer = status;
        },
    }
}