import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

//Routes
import authRoutes from './modules/auth'
import userRoutes from './modules/users'

// Views - Pages
import Full from '../containers/Default'
import Page404 from '../views/errors/Page404'

export default new Router({
    mode: 'history',
    linkActiveClass: 'active',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: '/',
            redirect: '/dashboard',
            name: 'Home',
            component: Full,
            meta: {
                breadcrumb: 'Home'
            },
            children:
                [
                    ...userRoutes,
                    {
                        path: 'dashboard',
                        name: 'Dashboard',
                        component: require('../views/Dashboard').default,
                    },
                    {
                        path: 'account',
                        name: 'Account',
                        component: require('../views/users/Account').default
                    }
                ]
        },
        ...authRoutes,
        {
            path: '*',
            name: '404',
            component: Page404
        }
    ]
})
