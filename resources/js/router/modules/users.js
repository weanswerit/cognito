export default [
    {
        path: '/users',
        name: 'Users-Index',
        redirect: '/users/list',
        component: {
            render (c) { return c('router-view') }
        },
        children: [
            {
                path: 'list',
                name: 'Users',
                component: require('../../views/users/List').default
            },
            {
                path: 'create',
                name: 'Create User',
                component: require('../../views/users/Create').default
            },
            {
                path: 'edit/:userId',
                name: 'Edit User',
                component: require('../../views/users/Edit').default
            },
        ]
    }
]