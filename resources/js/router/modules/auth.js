export default [
    {
        path: '/login',
        name: 'Login',
        component: require('../../views/auth/Login').default,
    }, {
        path: '/register',
        name: 'Register',
        component: require('../../views/auth/Register').default,
    }, {
        path: '/password/reset',
        name: 'Password-Reset',
        component: require('../../views/auth/Password-Reset').default,
    }, {
        path: '/password/reset/:token/:email',
        name: 'Password-Change',
        component: require('../../views/auth/Password-Change').default,
    }
]