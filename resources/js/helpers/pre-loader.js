(function() {
    var fadeTarget = document.getElementById('preloader');
    setTimeout(function(){
        var fadeEffect = setInterval(function () {
            if (!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1;
            }
            if (fadeTarget.style.opacity > 0) {
                fadeTarget.style.opacity -= 0.5;
            } else {
                fadeTarget.parentNode.removeChild(fadeTarget);
                clearInterval(fadeEffect);
            }
        }, 100);
    }, 1500);
})();