@extends('emails::common.master')

@section('content')

    @include('emails::sections.text_button', [
        'heading' => 'Hi there,',
        'headingTextAlign' => 'left',
        'text' => 'You are receiving this email because we received a password reset request for your account.',
        'buttonUrl' => route('password.reset', ['token' => $token, 'email' => base64_encode($email)]),
        'buttonText' => 'Reset Password',
        'buttonAlign' => 'left'
    ])

    @include('emails::sections.text_block', [
        'text' => 'If you did not request a password reset, no further action is required.<br><br> Regards, <br> '. config('app.name'),
        'padding' => '0px 40px 40px'
    ])

    @include('emails::sections.hr', [
        'padding' => '0 20px'
    ])

    @include('emails::sections.text_block', [
        'text' => 'If you’re having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: '.
                \Wai\Emails\Email::link(route('password.reset', ['token' => $token, 'email' => base64_encode($email)]), route('password.reset', ['token' => $token, 'email' => base64_encode($email)])),
    ])

@endsection
