@extends('emails::common.master')

@section('content')

    @include('emails::sections.text_button', [
        'heading' => 'Hi there,',
        'headingTextAlign' => 'left',
        'text' => 'Please click the button below to verify your email address.',
        'buttonUrl' => $url,
        'buttonText' => 'Verify Email Address',
        'buttonAlign' => 'left'
    ])

    @include('emails::sections.text_block', [
        'text' => 'If you did not create an account, no further action is required.<br><br> Regards, <br> '. config('app.name'),
        'padding' => '0px 40px 40px'
    ])

    @include('emails::sections.hr', [
        'padding' => '0 20px'
    ])

    @include('emails::sections.text_block', [
        'text' => 'If you’re having trouble clicking the "Verify Email Address" button, copy and paste the URL below into your web browser: '.
                \Wai\Emails\Email::link($url, $url),
    ])

@endsection
