<!DOCTYPE html>
<html lang="en">
<head>
    @include('common.header')
</head>
<body  @if(!config('app.registration_enabled')) data-reg="false" @endif>

@include('common.preloader')

@if( $__env->yieldContent('body'))
    @yield('body')
@endif

@include('common.footer')
</body>
</html>